<?php

/**
 * @file commerce_fancourier.admin.inc
 * Admin setting and configuration forms for FanCourier.
 */

module_load_include('inc', 'commerce_fancourier', 'commerce_fancourier.api');

/**
 * FanCourier shipping settings form.
 */
function commerce_fancourier_settings_form() {
  $form = [];

  $user = _commerce_fancourier_get_user();

  # API Credentials
  $form['credentials'] = [
    '#type' => 'fieldset',
    '#title' => t('API Credentials'),
    '#collapsible' => TRUE,
    '#description' => t('In order to obtain shipping rate estimates and use the API, you must have an account with FanCourier. You can apply for FanCourier API credentials at !link ', ['!link' => l('selfawb.ro', 'https://www.selfawb.ro')]
    ),
  ];

  $form['credentials']['commerce_fancourier_user'] = [
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#description' => t(''),
    '#required' => TRUE,
    '#default_value' => $user['username'],
  ];

  $form['credentials']['commerce_fancourier_pass'] = [
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#required' => TRUE,
    '#default_value' => $user['user_pass'],
  ];

  $form['credentials']['commerce_fancourier_clid'] = [
    '#type' => 'textfield',
    '#title' => t('Client ID'),
    '#description' => t('FAN Courier client ID'),
    '#required' => TRUE,
    '#default_value' => $user['client_id'],
  ];

  // If services show on admin page.
  $services = NULL;
  if ($user) {
    $services = _commerce_fancourier_get_services($user);
  }

  # Shipping services
  if ($services) {
    $form['services'] = [
      '#type' => 'fieldset',
      '#title' => t('Shipping services'),
      '#collapsible' => TRUE,
      '#description' => t('Enable and configure the services.'),
    ];

    $settings = variable_get('commerce_fancourier_expedition_payment_settings', []);
    $fancourier_services_checkboxes = [];
    foreach ($services as $key => $service) {
      if (isset($settings[$service]) && $settings[$service]) {
        $status = 'Overwritten';
      }
      else {
        $status = '';
      }
      $fancourier_services_checkboxes[$service] = [$service, l('Configure', 'admin/commerce/config/shipping/methods/fancourier/service/' . $key . '/config'), $status];
    }
    $form['services']['commerce_fancourier_services'] = array(
      '#type' => 'tableselect',
      '#header' => ['Service', 'Operation', 'Status'],
      '#options' => $fancourier_services_checkboxes,
      '#multiple' => TRUE,
      '#default_value' => variable_get('commerce_fancourier_services', []),
    );
    $form['services']['defaults'] = [
      '#type' => 'fieldset',
      '#title' => t('Default settings'),
      '#collapsible' => TRUE,
      '#description' => t('Default settings for the unconfigured services.'),
    ];
    $form['services']['defaults']['commerce_fancourier_expedition_payment_default'] = [
      '#type' => 'select',
      '#title' => t('Payment of shipment at'),
      '#required' => TRUE,
      '#options' => [NULL => t('- Select -'), 'expeditor' => t('Sender'), 'destinatar' => t('Recipient')],
      '#default_value' => variable_get('commerce_fancourier_expedition_payment_default', []),
      '#description' => t('Specify which party pays the shipping costs.'),
    ];
    $form['services']['defaults']['commerce_fancourier_expedition_reimbursement_default'] = [
      '#type' => 'select',
      '#title' => t('Transport payment of reimbursement at'),
      '#required' => TRUE,
      '#options' => [NULL => t('- Select -'), 'expeditor' => t('Sender'), 'destinatar' => t('Recipient')],
      '#default_value' => variable_get('commerce_fancourier_expedition_payment_default', []),
      '#description' => t('Reimbursment payment represents the fee that is charged for the AWB issued on destination, in order to refund the amount / documents / nondocuments from the reimbursment line.'),
    ];
    $form['services']['defaults']['commerce_fancourier_expedition_insurance'] = [
      '#type' => 'checkbox',
      '#title' => t('Insurance'),
      '#default_value' => variable_get('commerce_fancourier_expedition_insurance', 0),
      '#description' => t('Insurance represents 1% of the package(s) value.This amount will be added to the final price. Example: From city x to y the shipping cost 16.50 lei and the value of order is 100 lei. 1% of 100 is 1 lei then the final price will be 17.50.'),
    ];
  }
  else {
    return system_settings_form($form);
  }

  # Package Content
  $form['package_content'] = [
    '#type' => 'fieldset',
    '#title' => t('Package Content'),
    '#collapsible' => TRUE,
    '#description' => t('Fan Courier requires a package size when determining estimates. Uses metric unites.'),
  ];
  $form['package_content']['commerce_fancourier_package_types'] = [
    '#type' => 'checkboxes',
    '#options' => ['envelope' => t('Envelope'), 'package' => t('Package')],
    '#title' => t('Package type'),
    '#default_value' => variable_get('commerce_fancourier_package_types', []),
    '#required' => TRUE,
  ];
  $form['package_content']['commerce_fancourier_package_weight'] = [
    '#type' => 'textfield',
    '#title' => t('Weight'),
    '#default_value' => variable_get('commerce_fancourier_package_weight', 1),
    '#required' => TRUE,
    '#description' => 'kg',
    '#element_validate' => array('element_validate_integer_positive'),
  ];
  $form['package_content']['commerce_fancourier_package_height'] = [
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => variable_get('commerce_fancourier_package_height', 1),
    '#required' => TRUE,
    '#description' => 'cm',
    '#element_validate' => array('element_validate_integer_positive'),
  ];
  $form['package_content']['commerce_fancourier_package_width'] = [
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => variable_get('commerce_fancourier_package_width', 1),
    '#required' => TRUE,
    '#description' => 'cm',
    '#element_validate' => array('element_validate_integer_positive'),
  ];
  $form['package_content']['commerce_fancourier_package_length'] = [
    '#type' => 'textfield',
    '#title' => t('Length'),
    '#default_value' => variable_get('commerce_fancourier_package_length', 1),
    '#required' => TRUE,
    '#description' => 'cm',
    '#element_validate' => array('element_validate_integer_positive'),
  ];
  $form['package_content']['details'] = array(
    '#markup' => t('The package size is used to determine the number of packages necessary to
      create a FanCourier shipping cost estimate. <strong>If products do not have physical dimensions and
      weights associated with them, the estimates will not be accurate.</strong> The logic implemented works as:
      <ul>
      <li>Assume each order requires at least one package.</li>
      <li>Use the combined volume of all products in an order to determine the number of packages.</li>
      </ul>
      This is a simple calculation that can get close to actual shipping costs. More
      complex logic involving multiple package sizes, weights, and void space can be implemented via
      custom modules.
      <br/><br/>
      For more information read FanCourier\'s ' . l('general conditions', 'http://www.fancourier.ro/en/tools/termeni-si-conditii-fancourier/', array('attributes' => array('target' => '_blank'))) . '.'),
  );

  $form['debug'] = [
    '#type' => 'fieldset',
    '#title' => t('Debug'),
    '#collapsible' => TRUE,
    '#description' => t('Debug mode for developers.'),
  ];
  $form['debug']['commerce_fancourier_debug'] = [
    '#type' => 'checkbox',
    '#title' => t('Debug'),
    '#default_value' => variable_get('commerce_fancourier_debug', 0),
    '#description' => t('Show useful error messages if something went wrong or you didn`t configured correctly the module.<br />'
        . 'Use this only at development stage.'),
  ];

  return system_settings_form($form);
}

/**
 * Services settings form.
 */
function commerce_fancourier_service_form($form, &$form_state, $service_id) {

  $form = $values = [];
  $settings = variable_get('commerce_fancourier_expedition_payment_settings', []);
  $services = _commerce_fancourier_get_services(_commerce_fancourier_get_user());
  $name = $services[$service_id];

  $form['payment_at'] = [
    '#type' => 'select',
    '#title' => t('Payment of shipment at'),
    '#options' => [0 => t('- Use default -'), 'expeditor' => t('Sender'), 'destinatar' => t('Recipient')],
    '#default_value' => isset($settings[$name]['payment']) ? $settings[$name]['payment'] : NULL,
    '#description' => t('Specify which party pays the shipping costs.'),
  ];

  $form['transport_payment_reimbursement'] = [
    '#type' => 'select',
    '#title' => t('Transport payment of reimbursement at'),
    '#options' => [0 => t('- Use default -'), 'expeditor' => t('Sender'), 'destinatar' => t('Recipient')],
    '#default_value' => isset($settings[$name]['reimbursement']) ? $settings[$name]['reimbursement'] : NULL,
    '#description' => t('Reimbursment payment represents the fee that is charged for the AWB issued on destination, in order to refund the amount / documents / nondocuments from the reimbursment line.'),
  ];

  $form['expedition_insurance'] = [
    '#type' => 'select',
    '#title' => t('Insurance'),
    '#options' => ['insurance' => t('Insurance')],
    '#options' => [NULL => t('- Use default -'), 1 => t('With insurance'), 0 => t('Without insurance')],
    '#default_value' => isset($settings[$name]['insurance']) ? $settings[$name]['insurance'] : NULL,
    '#description' => t('Insurance represents 1% of the package(s) value.This amount will be added to the final price. Example: From city x to y the shipping cost 16.50 lei and the value of order is 100 lei. 1% of 100 is 1 lei then the final price will be 17.50.'),
  ];

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  $form_state['service_name'] = $name;
  $form_state['payment_settings'] = $settings;
  return $form;
}

/**
 * Submit callback from commerce_fancourier_service_form().
 */
function commerce_fancourier_service_form_submit($form, &$form_state) {

  if (is_numeric($form_state['values']['payment_at'])) {
    unset($form_state['payment_settings'][$form_state['service_name']]['payment']);
  }
  else {
    $form_state['payment_settings'][$form_state['service_name']]['payment'] = $form_state['values']['payment_at'];
  }
  if (is_numeric($form_state['values']['transport_payment_reimbursement'])) {
    unset($form_state['payment_settings'][$form_state['service_name']]['reimbursement']);
  }
  else {
    $form_state['payment_settings'][$form_state['service_name']]['reimbursement'] = $form_state['values']['transport_payment_reimbursement'];
  }
  if (is_numeric($form_state['values']['expedition_insurance'])) {
    $form_state['payment_settings'][$form_state['service_name']]['insurance'] = $form_state['values']['expedition_insurance'];
  }
  else {
    unset($form_state['payment_settings'][$form_state['service_name']]['insurance']);
  }
  variable_set('commerce_fancourier_expedition_payment_settings', $form_state['payment_settings']);
}
