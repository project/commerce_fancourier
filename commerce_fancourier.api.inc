<?php

/**
 * @file commerce_fancourier.api.inc
 * FanCourier shipping global functions.
 */

/**
 * FAN Courier user credentials
 *
 * @staticvar array $user
 *   User credentials.
 *
 * @return array
 */
function _commerce_fancourier_get_user() {
  $user = &drupal_static(__FUNCTION__, NULL);
  if (!$user) {
    $name = variable_get('commerce_fancourier_user');
    $pass = variable_get('commerce_fancourier_pass');
    $clid = variable_get('commerce_fancourier_clid');
    if (is_string($name) && is_string($pass) && is_string($clid)) {
      $user = ['username' => $name, 'user_pass' => $pass, 'client_id' => $clid];
    }
    else {
      return FALSE;
    }
  }
  return $user;
}

/**
 * Get FanCourier services.
 *
 * @param type $user
 *   Fancourier user.
 *
 * @return array
 */
function _commerce_fancourier_get_services($user) {

  if ($services = cache_get('fancourier:services')) {
    return $services->data;
  }

  try {
    $fc = new \FanCourier\fanCourier();
    $endpoint = $fc->getEndpoint('Servicii');
    $endpoint->setParams($user);
    $endpoint->noHeader();
    $services = $endpoint->getResult();
    if ($services) {
      cache_set('fancourier:services', $services);
    }
    return $services;
  }
  catch (Exception $exc) {
    drupal_set_message(t('Something went wrong with the FanCourier API. Check log messages.'), 'warning');
    watchdog('commerce_fancourier', $exc->getMessage());
  }
  return [];
}

/**
 * Get a list of cities per county from cache.
 * If the mentioned cache item does not exist, load
 * resources from API and cache results.
 */
function _commerce_fancourier_get_cities($county, $city) {

  $county = _commerce_fancourier_clean_address_name($county);
  $city_list = cache_get('fancourier:' . $county);

  if (!isset($city_list->data)) {
    try {
      $fc = new \FanCourier\fanCourier();
      $endpoint = $fc->getEndpoint('Localitati');
      $endpoint->setParams(_commerce_fancourier_get_user());
      $endpoint->noHeader();
      $city_list = _commerce_fancourier_process_cities($endpoint->getResult());
      cache_set('fancourier:' . $county, $city_list);
      return $city_list;
    }
    catch (Exception $exc) {
      drupal_set_message(t('Something went wrong with the FanCourier API. Check log messages.'), 'warning');
      watchdog('commerce_fancourier', $exc->getMessage());
    }
  }
  return $city_list->data;
}

/**
 * Process list of cities before caching.
 */
function _commerce_fancourier_process_cities($city_list) {
  $processed_city_list = [];
  foreach ($city_list as $city_info) {
    $info = explode(',', $city_info);
    $processed_city_list[_commerce_fancourier_clean_address_name($info[1])] = $city_info;
  }
  return $processed_city_list;
}

function _set_debug_message($message, $type = 'warning') {
  $_SESSION['commerce_fancourier_debug'][] = [
    'type' => $type,
    'text' => $message
  ];
}

function _get_debug_message() {
  if (COMMERCE_FANCOURIER_DEBUG && isset($_SESSION['commerce_fancourier_debug'])) {
    foreach ($_SESSION['commerce_fancourier_debug'] as $message) {
      drupal_set_message($message['text'], $message['type']);
    }
  }
  $_SESSION['commerce_fancourier_debug'] = NULL;
}
