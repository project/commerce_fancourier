Description
-----------
The module connects the FAN Courier [RO] shipping service API to the commerce
functionality to deal with shipping.

Installation
------------
To install this module, do the following:

1. Download & install the module.

2. For managing dependencies, download & install https://www.drupal.org/project/composer_manager.

3. Go to sites/default/files/composer and run "composer install".

Configuration
-------------
To enable and configure this module do the following:

1. Go to Admin -> Shipping -> Shipping methods -> FAN Courier > Edit.
   Fill in connection information (Username, Password and Key) and enable
   desired shipping methods.
    (Link available if Shipping UI module is enabled)

2. Go to Admin > Store > Customer profiles > Customer profiles > Shipping information "manage fields" > Address "edit"
   Set Format handlers - check "Address form (Romania add-on)"

3. Go to Admin > Store > Configuration > Currency settings
   Set default currency to RON
    (Store links available if Customer Profile UI module is enabled)