<?php

/**
 * @file commerce_fancourier_awb.admin.inc
 * Admin setting and configuration forms for FanCourier AWB.
 */

/**
 * FanCourier shipping settings form.
 */
function commerce_fancourier_awb_settings_form() {
  global $base_path;
  $form = [];

  $rule_id = rules_config_load('commerce_fancourier_awb_generate')->id;

  $form['commerce_fancourier_awb_auto'] = [
    '#type' => 'checkbox',
    '#title' => t('Generate'),
    '#description' => t('If enabled will genarate awb`s automaticaly.'),
    '#default_value' => variable_get('commerce_fancourier_awb_auto', 0),
  ];

  $options = [0 => t('Rule')];
  $description = t('Configure !link option.', ['!link' => l('Rule', 'admin/config/workflow/rules/reaction/manage/' . $rule_id)]) . '<br />';

  if (module_exists('elysia_cron')) {
    $options[1] = t('Cron');
    $description .= t('Configure !link option.', ['!link' => l('Cron', 'admin/config/system/cron/settings')]) . '<br />';
  }

  $form['commerce_fancourier_awb_rule'] = [
    '#type' => 'radios',
    '#title' => t('Generate using'),
    '#options' => $options,
    '#default_value' => variable_get('commerce_fancourier_awb_rule', 0),
    '#description' => $description,
  ];

  $form['help'] = [
    '#type' => 'item',
    '#title' => t('Help'),
    '#markup' => t('To access the config for Rules enable the !rule.<br />'
        . 'Enable !cron module for more option.', [
      '!rule' => l('Rules UI', 'https://www.drupal.org/project/rules'),
      '!cron' => l('Elysia cron', 'https://www.drupal.org/project/elysia_cron')
        ]
    ),
  ];

  return system_settings_form($form);
}
