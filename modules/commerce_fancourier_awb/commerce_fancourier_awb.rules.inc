<?php

/**
 * @file
 * Rules integration for fancourier.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_fancourier_awb_rules_action_info() {
  $actions = array();

  $actions['commerce_fancourier_awb'] = array(
    'label' => t('Generate AWB'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order in checkout'),
      ),
    ),
    'group' => t('Commerce Checkout'),
    'callbacks' => array(
      'execute' => '_commerce_fancourier_awb_generate',
    ),
  );

  return $actions;
}

/**
 * Get AWB number based on shipping service and address.
 */
function _commerce_fancourier_awb_generate($order) {
  $profile = commerce_customer_profile_load($order->commerce_customer_shipping[LANGUAGE_NONE][0]['profile_id']);

  $li_ids = [];
  foreach ($order->commerce_line_items[LANGUAGE_NONE] as $key => $value) {
    $li_ids[$value['line_item_id']] = $value['line_item_id'];
  }
  $line_items = commerce_line_item_load_multiple($li_ids, ['type' => 'shipping', 'order_id' => $order->order_id]);
  $service = reset($line_items)->commerce_shipping_service[LANGUAGE_NONE][0]['value'];
  $params = _commerce_fancourier_get_user();

  try {
    $fc = new \FanCourier\fanCourier();
    $endpoint = $fc->getEndpoint('awbGenerator');
    $endpoint->createFile();
    $endpoint->addNewItem(_commerce_fancourier_item_from_address($service, $profile->commerce_customer_address[LANGUAGE_NONE][0], $order));
    $params['fisier'] = $endpoint->getFile();
    $endpoint->setParams($params);
    dpm($endpoint);
    return;
    $result = $endpoint->getResult();
    $awb = str_getcsv((reset($result)));
    _commerce_fancourier_add_awb($awb, $order->order_id);
  }
  catch (Exception $exc) {
    drupal_set_message(t('Something went wrong with the FanCourier API. Check log messages.'), 'warning');
    watchdog('commerce_fancourier', $exc->getMessage());
  }
}

function _commerce_fancourier_add_awb($awb, $order_id) {
  if ($awb[1] == 1) {
    db_insert('fancourier')
        ->fields(array(
          'order_id' => $order_id,
          'awb' => $awb[2],
          'status' => $awb[1],
          'created' => REQUEST_TIME,
          'changed' => REQUEST_TIME,
        ))
        ->execute();
  }
  else {
    db_insert('fancourier_log')
        ->fields(array(
          'order_id' => $order_id,
          'log' => serialize($awb),
        ))
        ->execute();
  }
}

/**
 * 
 */
// Under construction ...
function _commerce_fancourier_item_from_address($service, $address, $order) {
  $package = variable_get('commerce_fancourier_package_types', []);
  $item = \FanCourier\Plugin\csv\csvItem::newItem();
  $counties = addressfield_ro_get_counties();
  $item->setItem('tip', $service);
  $item->setItems([
    'localitate' => _commerce_fancourier_clean_address_name($address['locality']),
    'judet' => _commerce_fancourier_clean_address_name($counties[$address['administrative_area']]),
    'strada' => _commerce_fancourier_clean_address_name($address['thoroughfare']),
    'nr' => '',
    'telefon' => $address['phone_number'],
    'nume_destinatar' => $address['name_line'],
    'plata_expeditii' => 'destinatar',
  ]);

  _commerce_fancourier_item_payment_calc($item, $service, $order);

  if (isset($package['envelope']) && $package['envelope'] !== 0) {
    $item->setItem('nr_plic', 1);
  }
  if (isset($package['package']) && $package['package'] !== 0) {
    $item->setItem('nr_colet', 1);
  }

  $item->setItem('greutate', variable_get('commerce_fancourier_package_weight', 1));
  $item->setItem('inaltime_pachet', variable_get('commerce_fancourier_package_height', 1));
  $item->setItem('latime_pachet', variable_get('commerce_fancourier_package_width', 1));
  $item->setItem('lungime_pachet', variable_get('commerce_fancourier_package_length', 1));

  print_r($item);
  die;

  return $item;
}

function _commerce_fancourier_item_payment_calc(&$item, $service, $order) {

  $payments = _commerce_fancourier_expedition_payments($service);
  if (isset($payments['payment'])) {
    $item->setItem('plata_expeditii', $payments['payment']);
  }
  if (isset($payments['reimbursement'])) {
    $item->setItem('plata_ramburs_la', $payments['reimbursement']);
  }

  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $amount = $wrapper->commerce_order_total->data->value()['components'][0]['price']['amount'];
  $price = commerce_currency_amount_to_decimal($amount, $wrapper->commerce_order_total->data->value()['components'][0]['price']['currency_code']);
  $item->setItem('ramburs', $price);
  if (isset($payments['insurance']) && $payments['insurance'] == 1) {
    $item->setItem('valoare', $price);
  }
}
