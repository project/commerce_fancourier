<?php

/**
 * @file
 * Default rules configurations for Fan Courier.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_fancourier_awb_default_rules_configuration() {

  $rules = [];
  $rule = rules_reaction_rule();

  $rule->label = t('FanCourier generate AWB');
  $rule->active = TRUE;
  $rule->event('commerce_order_update')->condition('data_is', [
    'data:select' => 'commerce-order:status',
    'op' => '==',
    'value' => 'completed',
  ])->action('commerce_fancourier_awb', [
    'commerce_order:select' => 'commerce-order',
  ]);
  $rules['commerce_fancourier_awb_generate'] = $rule;
  return $rules;
}
